const http = require('http');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');


const server = http.createServer((req, res) => {
    if (req.method === 'GET'){
        if (req.url === '/html') {
            fs.readFile('./public/htmlFile.html','UTF-8', (err, data) => {
            if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
            } 
            else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.end(data);
            }
            });
        } 
        else if (req.url === '/json') {
            fs.readFile('./public/jsonFile.json', (err, data) => {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'text/plain' });
                    res.end('Internal Server Error');
                } 
                else {
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.end(data);
                }
            });
        } 
        else if(req.url === '/uuid') {
            const content= uuidv4();
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(content);
        }
        else if (req.url.startsWith('/status/')) {
            const statusCode = parseInt(req.url.split('/')[2]);
            res.writeHead(statusCode, { 'Content-Type': 'text/plain' });
            res.end(`Response with status code ${statusCode}`);
          }
        else if (req.url.startsWith('/delay/')) {
            const delaySeconds = parseInt(req.url.split('/')[2]);
            setTimeout(() => {
              res.writeHead(200, { 'Content-Type': 'text/plain' });
              res.end(`Response with 200 status code after ${delaySeconds} seconds delay`);
            }, delaySeconds * 1000);
          }
        else {
            res.writeHead(404, { 'Content-Type': 'text/plain' })
            res.end('404 Not Found')
        }
}
    })

    server.listen(3000, () => {
        console.log(`Server running at http://localhost:3000/`)
})
